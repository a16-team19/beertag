## **Beer Tag**

Beer Tag is web application that enables your users to manage all the beers that they have drank and want to drink. Every user can add beers. Also, Beer Tag allows you to sort, rate a beer and see average rating from different users. Administrators have full control over all beers and all users.Each beer has detailed information about it from the ABV (alcohol by volume) to the style and description.

## 
  - Visible without authentication
    - the application start page
    - the user login form
    - the user registration form
    - browse all beers and their details
    - filter by origin country, style, brewery, style, tags, ABV, rating
    - the about page

    
  - When authenticated, user can create new beers, add a beer to his wish list and drunk list and rate a beer. They can also edit their own beers. Registered users can modify their personal information as well.

  - The app have profile page that shows user’s profile picture, first name, last name, email and information about wish list and drank list
 
  - The admin page shows multiple tables with beers, users, breweries, countries, styles, tags supporting the CRUD operations.

### Technologies
- *IntelliJ IDEA* - as a IDE during the whole development process
- *Spring MVC* - as an application framework
- *Bootstrap 4* - for the front-end of the application making it responsive and providing ready to use stylesheets
- *Thymeleaf* - as a template engine in order to load a big amount of data (beers, users). Also thymeleaf allows inserting and replacing views and imports, which makes the code way more readable and replaceable
- *MySQL* - as database
- *Hibernate* - access the database
- *Spring Security* - for managing users, roles and authentication
- *Git* - for source control management and documentation / manual of the application


### Routes

| Mapping | Description |
| ------ | ------ |
| / | Home page with search bar and top 4 beers by avg. rating |
| /beers| Beer page where all the beers are shown and there is filter options|
| /beers/{beerName} | Beer details page holding details of beer with name of {beerName} |
| /beers/createBeer | Create beer |
| /register | Register page with three must-fill text-boxes|
| /profile | User page providing information about the user |
| /login | Login page providing authentication via username and password|
| /login?logout=true | Users are logged out and redirected to login page |
| /admin/beers | Admin page where you can do the CRUD operation over beers|
| /admin/users | Admin page where you can do the CRUD operation over users|
| /admin/{field}/new | Admin page where you can create new breweries, countries, styles, tags, where field is breweries, countries, styles, tags|
| /admin/{field} | Admin page where you can delete breweries, countries, styles, tags, where field is breweries, countries, styles, tags|
| /about | About page|

### Screenshots

- Home without authentication
![home-not-authenticated](/images/homepage_not_auth.png)

- Home with authentication
![logged-home](/images/homepage_auth.png)

- Beers tab
![beer tab](/images/beertab.png)

- Beer details
![beer-details](/images/details.png)

- Create Beer
![create-beer](/images/create.png)

- User profile
![user-profile](/images/profile.png)

- Admin Beer tab
![admin-panel](/images/adminbeer.png)

- Edit Profile
![update-profile](/images/editprofile.png)

- Update beer
![update-beer](/images/updatebeer.png)

- Login
![login](/images/login.png)

- Register page
![register](/images/register.png)