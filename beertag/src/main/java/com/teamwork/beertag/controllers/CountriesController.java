package com.teamwork.beertag.controllers;

import com.teamwork.beertag.models.Country;
import com.teamwork.beertag.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class CountriesController {
    private final CountryService countryService;

    @Autowired
    public CountriesController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping("/admin/countries")
    public ModelAndView showCountries() {
        ModelAndView mav = new ModelAndView("countries");
        mav.addObject("countries", countryService.getAll());
        return mav;
    }

    @GetMapping("/admin/countries/new")
    public ModelAndView showCreateCountryForm() {
        ModelAndView mav = new ModelAndView("createCountry");
        mav.addObject("country", new Country());
        return mav;
    }


    @PostMapping("/admin/countries/new")
    public String createCountry(@Valid @ModelAttribute Country country, BindingResult errors,
                                Model model) {

        if (errors.hasErrors()) {
            return "createCountry";
        }
        if (countryService.checkCountryExist(country.getName())) {
            model.addAttribute("error",
                    "Country with that name already exist!");

            return "createCountry";
        }

        countryService.createCountry(country);

        return "redirect:/admin/countries";
    }

    @PostMapping("/admin/countries/delete/{id}")
    public String deleteCountry(@PathVariable String id) {
        Country country = countryService.getById(id);
        countryService.deleteCountry(country);

        return "redirect:/admin/countries";
    }
}
