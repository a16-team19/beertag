package com.teamwork.beertag.controllers.rest;

import com.teamwork.beertag.models.Country;
import com.teamwork.beertag.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/countries")
public class CountryController {

    private final CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping
    public List<Country> getAll() {
        return countryService.getAll();
    }

    @GetMapping("/{id}")
    public Country getById(@PathVariable String id) {
        return countryService.getById(id);
    }

    @PostMapping
    public void createCountry(@RequestBody Country country) {
        countryService.createCountry(country);
    }

    @PutMapping
    public void updateCountry(@RequestBody Country country) {
        countryService.updateCountry(country);
    }

    @DeleteMapping
    public void deleteCountry(@RequestBody Country country) {
        countryService.deleteCountry(country);
    }
}

