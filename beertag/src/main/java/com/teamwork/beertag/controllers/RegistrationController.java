package com.teamwork.beertag.controllers;

import com.teamwork.beertag.models.User;
import com.teamwork.beertag.models.dtos.DtoMapper;
import com.teamwork.beertag.models.dtos.UserRegisterDto;
import com.teamwork.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;
    private final DtoMapper mapper;
    private final UserService userService;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, DtoMapper mapper, UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.mapper = mapper;
        this.userService = userService;
    }

    @GetMapping("/register")
    public ModelAndView showRegisterPage() {
        ModelAndView mav = new ModelAndView("register");
        mav.addObject("user", new UserRegisterDto());
        return mav;
    }

    @PostMapping("/register")
    public ModelAndView registerUser(@Valid UserRegisterDto userRegisterDto, BindingResult bindingResult) {
        ModelAndView mav = new ModelAndView();

        if (bindingResult.hasErrors()) {
            mav.setViewName("register");
            mav.addObject("user", new UserRegisterDto());
            return mav;
        }
        if (userDetailsManager.userExists(userRegisterDto.getUsername())) {
            mav.addObject("error", "User with the same username already exists!");
            mav.addObject("user", new UserRegisterDto());
            mav.setViewName("register");
            return mav;
        }

        if (!userRegisterDto.getPassword().equals(userRegisterDto.getPasswordConfirmation())) {
            mav.addObject("error", "Password does't match!");
            mav.addObject("user", new UserRegisterDto());
            mav.setViewName("register");
            return mav;
        }
        mav.setViewName("login");
        User user = mapper.fromUserRegisterDto(userRegisterDto);
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser = new org.springframework.security.core.userdetails.User(
                user.getUsername(), passwordEncoder.encode(user.getPassword()), authorities);
        userDetailsManager.createUser(newUser);
        userService.addDefaultUserDetails(user.getUsername());
        return mav;
    }
}
