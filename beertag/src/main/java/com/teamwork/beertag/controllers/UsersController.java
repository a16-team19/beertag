package com.teamwork.beertag.controllers;

import com.teamwork.beertag.models.User;
import com.teamwork.beertag.models.dtos.DtoMapper;
import com.teamwork.beertag.models.dtos.UserDetailsDto;
import com.teamwork.beertag.services.contracts.BeersService;
import com.teamwork.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class UsersController {

    private final UserService userService;
    private final BeersService beersService;
    private final DtoMapper mapper;

    @Autowired
    public UsersController(UserService userService, BeersService beersService, DtoMapper mapper) {
        this.userService = userService;
        this.beersService = beersService;
        this.mapper = mapper;
    }

    @GetMapping(value = {"/profile"})
    public ModelAndView showService(Principal principal) {
        ModelAndView mav = new ModelAndView("profile");
        mav.addObject("user", userService.getUserByUsername(principal.getName()));
        mav.addObject("beersWish", beersService.getWishList(principal.getName()));
        mav.addObject("beersDrank", beersService.getDrankList(principal.getName()));
        return mav;
    }

    @GetMapping("/profile/updateDetails")
    public ModelAndView showUpdateDetailsForm(Principal principal) {
        User user = userService.getUserByUsername(principal.getName());
        ModelAndView mav = new ModelAndView("updateUserDetails");
        mav.addObject("details", mapper.toUserDetailsDto(user.getUserDetails()));

        return mav;
    }

    @PostMapping("/profile/updateDetails")
    public String updateUserDetails(@ModelAttribute UserDetailsDto userDetailsDto, Principal principal) {
        userService.updateUserDetails(principal.getName(), userDetailsDto);

        return "redirect:/profile";
    }

    @GetMapping("/admin/users")
    public ModelAndView showUsers() {
        ModelAndView mav = new ModelAndView("users");
        mav.addObject("users", userService.getAll());

        return mav;
    }

    @PostMapping("/admin/users/delete/{name}")
    public String deleteUser(@PathVariable String name) {
        User user = userService.getUserByUsername(name);
        userService.deleteUser(user);

        return "redirect:/admin/users";
    }
}
