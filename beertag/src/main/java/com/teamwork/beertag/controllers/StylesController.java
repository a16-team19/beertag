package com.teamwork.beertag.controllers;

import com.teamwork.beertag.models.Style;
import com.teamwork.beertag.services.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class StylesController {

    private final StyleService styleService;

    @Autowired
    public StylesController(StyleService styleService) {
        this.styleService = styleService;
    }

    @GetMapping("/admin/styles")
    public ModelAndView showStyles() {
        ModelAndView mav = new ModelAndView("styles");
        mav.addObject("styles", styleService.getAll());

        return mav;
    }

    @GetMapping("/admin/styles/new")
    public ModelAndView showCreateStyleForm() {
        ModelAndView mav = new ModelAndView("createStyle");
        mav.addObject("style", new Style());

        return mav;
    }

    @PostMapping("/admin/styles/new")
    public String createStyle(@Valid @ModelAttribute Style style, BindingResult errors,
                              Model model) {

        if (errors.hasErrors()) {
            return "createStyle";
        }

        if (styleService.checkStyleExist(style.getName())) {
            model.addAttribute("error",
                    "Style with that name already exist!");

            return "createStyle";
        }

        styleService.createStyle(style);

        return "redirect:/admin/styles";
    }

    @PostMapping("/admin/styles/delete/{id}")
    public String deleteStyle(@PathVariable int id) {
        Style style = styleService.getById(id);
        styleService.deleteStyle(style);

        return "redirect:/admin/styles";
    }
}
