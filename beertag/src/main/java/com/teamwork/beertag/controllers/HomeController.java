package com.teamwork.beertag.controllers;

import com.teamwork.beertag.models.dtos.BeerSortDto;
import com.teamwork.beertag.models.dtos.DtoMapper;
import com.teamwork.beertag.models.dtos.BeerRatingDto;
import com.teamwork.beertag.models.dtos.RatingDto;
import com.teamwork.beertag.services.contracts.BeersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;

@Controller
public class HomeController {
    private final BeersService beersService;

    @Autowired
    public HomeController(BeersService beersService) {
        this.beersService = beersService;
    }

    @GetMapping(value = {"/", "/home"})
    public ModelAndView showHome(Principal principal) {
        ModelAndView mav = new ModelAndView("index");
        String username = principal != null ? principal.getName() : "";
        mav.addObject("beers", beersService.getTop4Beers(username));
        return mav;
    }

    @GetMapping(value = {"/about"})
    public ModelAndView showAboutUs() {
        return new ModelAndView("aboutus");
    }
}
