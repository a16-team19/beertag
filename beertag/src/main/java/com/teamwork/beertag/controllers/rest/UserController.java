package com.teamwork.beertag.controllers.rest;

import com.teamwork.beertag.exceptions.DuplicateEntityException;
import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.User;
import com.teamwork.beertag.models.dtos.DtoMapper;
import com.teamwork.beertag.models.dtos.UserDetailsDto;
import com.teamwork.beertag.models.dtos.UserRegisterDto;
import com.teamwork.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/v1/users")
public class UserController {

    private final UserService userService;
    private final DtoMapper mapper;

    @Autowired
    public UserController(UserService userService, DtoMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<User> getAll() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public User getByID(@PathVariable("id") String id) {
        try {
            return userService.getUserById(Integer.parseInt(id));
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        } catch (NumberFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping
    public User createUser(@RequestBody @Valid UserRegisterDto userDto) {
        try {
            User user = mapper.fromUserRegisterDto(userDto);
            return userService.createUser(user);
        } catch (DuplicateEntityException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

    @PutMapping
    public void updateUser(@RequestBody User user) {
        try {
            userService.updateUserById(user);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @DeleteMapping
    public void deleteUser(@RequestBody User user) {
        try {
            userService.deleteUser(user);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @PostMapping("/updateDetails")
    public void updateUserDetails(@RequestBody UserDetailsDto userDetailsDto, Principal principal) {
        try {
            userService.updateUserDetails(principal.getName(), userDetailsDto);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }
}

