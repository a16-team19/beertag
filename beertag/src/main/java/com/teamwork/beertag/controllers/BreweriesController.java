package com.teamwork.beertag.controllers;

import com.teamwork.beertag.models.Brewery;
import com.teamwork.beertag.services.contracts.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class BreweriesController {
    private final BreweryService service;

    @Autowired
    public BreweriesController(BreweryService service) {
        this.service = service;
    }

    @GetMapping("/admin/breweries")
    public ModelAndView showBreweries() {
        ModelAndView mav = new ModelAndView("breweries");
        mav.addObject("breweries", service.getAll());

        return mav;
    }

    @GetMapping("/admin/breweries/new")
    public ModelAndView showCreateBreweryForm() {
        ModelAndView mav = new ModelAndView("createBrewery");
        mav.addObject("brewery", new Brewery());

        return mav;
    }

    @PostMapping("/admin/breweries/new")
    public String createBrewery(@Valid @ModelAttribute Brewery brewery, BindingResult errors,
                                Model model) {

        if (errors.hasErrors()) {
            return "createBrewery";
        }

        if (service.checkBreweryExist(brewery.getName())) {
            model.addAttribute("error", "Brewery with that name already exist!");

            return "createBrewery";
        }

        service.createBrewery(brewery);

        return "redirect:/admin/breweries";
    }


    @PostMapping("/admin/breweries/delete/{id}")
    public String deleteBrewery(@PathVariable int id) {
        Brewery brewery = service.getById(id);
        service.deleteBrewery(brewery);

        return "redirect:/admin/breweries";
    }
}
