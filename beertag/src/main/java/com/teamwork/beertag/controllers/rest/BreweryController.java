package com.teamwork.beertag.controllers.rest;


import com.teamwork.beertag.exceptions.DuplicateEntityException;
import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.Brewery;
import com.teamwork.beertag.services.contracts.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/breweries")
public class BreweryController {

    private final BreweryService service;


    @Autowired
    public BreweryController(BreweryService service) {
        this.service = service;
    }

    @GetMapping
    public List<Brewery> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Brewery getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Brewery create(@RequestBody @Valid Brewery brewery) {
        try {
            return service.createBrewery(brewery);
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public void update(@RequestBody @Valid Brewery brewery) {
        try {
            service.updateBrewery(brewery);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping
    public void delete(@RequestBody Brewery brewery) {
        try {
            service.deleteBrewery(brewery);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}

