package com.teamwork.beertag.controllers.rest;

import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.dtos.BeerDto;
import com.teamwork.beertag.models.dtos.BeerRatingDto;
import com.teamwork.beertag.models.dtos.DtoMapper;
import com.teamwork.beertag.services.contracts.BeersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/beers")
public class BeerController {
    private final BeersService service;
    private final DtoMapper mapper;

    @Autowired
    public BeerController(BeersService service, DtoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

/*    @GetMapping
    public List<BeerRatingDto> getAll(@RequestParam(defaultValue = "") String name,
                                      @RequestParam(defaultValue = "") String style,
                                      @RequestParam(defaultValue = "") String brewery,
                                      @RequestParam(defaultValue = "") String country,
                                      @RequestParam(defaultValue = "0.0") double abv) {


        return service.getAll(mapper.getSortBeer(name, style, brewery, country, abv));
    }*/



    @GetMapping("/name")
    public Beer getByName(@RequestParam String name) {
        try {
            return service.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }



    @PutMapping
    public void update(@RequestBody @Valid BeerDto beer) {
        try {
            service.updateBeer(beer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping
    public void delete(@RequestBody Beer beer) {
        try {
            service.deleteBeer(beer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
