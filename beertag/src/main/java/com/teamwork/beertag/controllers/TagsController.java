package com.teamwork.beertag.controllers;

import com.teamwork.beertag.models.Tag;
import com.teamwork.beertag.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class TagsController {

    private final TagService tagService;

    @Autowired
    public TagsController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping("/admin/tags")
    public ModelAndView showTags() {
        ModelAndView mav = new ModelAndView("tags");
        mav.addObject("tags", tagService.getAll());

        return mav;
    }

    @GetMapping("/admin/tags/new")
    public ModelAndView showCreateTagForm() {
        ModelAndView mav = new ModelAndView("createTag");
        mav.addObject("tag", new Tag());

        return mav;
    }

    @PostMapping("/admin/tags/new")
    public String createTag(@Valid @ModelAttribute Tag tag, BindingResult errors,
                            Model model) {

        if (errors.hasErrors()) {
            return "createTag";
        }

        if (tagService.checkTagExist(tag.getName())) {
            model.addAttribute("error", "Tag with that name already exist!");
            return "createTag";
        }

        tagService.createTag(tag);

        return "redirect:/admin/tags";
    }

    @PostMapping("/admin/tags/delete/{id}")
    public String deleteTag(@PathVariable int id) {
        Tag tag = tagService.getTagById(id);
        tagService.deleteTag(tag);

        return "redirect:/admin/tags";
    }
}
