package com.teamwork.beertag.controllers.rest;

import com.teamwork.beertag.exceptions.DuplicateEntityException;
import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.Style;
import com.teamwork.beertag.services.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/styles")
public class StyleController {

    private final StyleService styleService;

    @Autowired
    public StyleController(StyleService styleService) {
        this.styleService = styleService;
    }

    @GetMapping
    public List<Style> getAll() {
        return styleService.getAll();
    }

    @GetMapping("/{id}")
    public Style getById(@PathVariable("id") String id) {
        return styleService.getById(Integer.parseInt(id));
    }

    @PostMapping
    public Style createStyle(@RequestBody @Valid Style style) {
        try {
            return styleService.createStyle(style);
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public void updateStyle(@RequestBody Style style) {
        styleService.updateStyle(style);
    }

    @DeleteMapping
    public void deleteStyle(@RequestBody Style style) {
        styleService.deleteStyle(style);
    }

    @GetMapping("/{styleId}/beers")
    public List<Beer> getStyleBeers(@PathVariable int styleId) {
        try {
            return styleService.getByStyle(styleId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}