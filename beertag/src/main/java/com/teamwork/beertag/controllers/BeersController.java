package com.teamwork.beertag.controllers;

import com.teamwork.beertag.exceptions.DuplicateEntityException;
import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.*;
import com.teamwork.beertag.models.dtos.*;
import com.teamwork.beertag.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Controller
public class BeersController {

    private final StyleService styleService;
    private final BreweryService breweryService;
    private final CountryService countryService;
    private final BeersService beersService;
    private final RatingService ratingService;
    private final TagService tagService;
    private final DtoMapper mapper;

    @Autowired
    public BeersController(StyleService styleService, BreweryService breweryService, CountryService countryService, BeersService beersService, RatingService ratingService, TagService tagService, DtoMapper mapper) {
        this.styleService = styleService;
        this.breweryService = breweryService;
        this.countryService = countryService;
        this.beersService = beersService;
        this.ratingService = ratingService;
        this.tagService = tagService;
        this.mapper = mapper;
    }

    @GetMapping("/beers/createBeer")
    public ModelAndView showCreateProduct() {
        ModelAndView mav = new ModelAndView("createBeer");
        return createBeerMav(mav);
    }

    @PostMapping(value = "/beers/createBeer")
    public ModelAndView create(@Valid @ModelAttribute BeerDto beerDto, Principal principal,
                               BindingResult errors) {

        ModelAndView mav = new ModelAndView();
        if (errors.hasErrors()) {
            mav.setViewName("createBeer");
            mav.addObject("error", "Wrong input!");
            return createBeerMav(mav);
        }

        beerDto.setCreatedBy(principal.getName());
        try {
            beersService.createBeer(beerDto);
        } catch (DuplicateEntityException ex) {
            mav.setViewName("createBeer");
            mav.addObject("error", ex.getMessage());
            return createBeerMav(mav);
        }

        return new ModelAndView("redirect:/beers");
    }

    @GetMapping(value = "/beers")
    public ModelAndView showBeers(@RequestParam(defaultValue = "", name = "name") String beerName, Principal principal) {
        ModelAndView mav = new ModelAndView("beerList");
        String username = principal != null ? principal.getName() : "";
        List<BeerRatingDto> beers = beersService.filterBeersByName(beerName, username);
        return getModelAndView(mav, beers);
    }

    @GetMapping(value = "/beers/sort")
    public ModelAndView showSortedBeers(@Valid @ModelAttribute BeerSortDto beerSortDto, Principal principal) {
        ModelAndView mav = new ModelAndView("beerList");
        String username = principal != null ? principal.getName() : "";
        List<BeerRatingDto> beers = beersService.filterBeers(beerSortDto, username);
        return getModelAndView(mav, beers);
    }

    @PostMapping(value = "/beers/rate")
    public void rateBeer(@RequestBody RatingDto ratingDto, Principal principal, HttpServletResponse response) throws IOException {
        ratingDto.setUsername(principal.getName());
        try {
            ratingService.addRating(ratingDto);
        } catch (DuplicateEntityException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
        response.sendRedirect("/");
    }

    @DeleteMapping(value = "/beers/rate")
    public void deleteBeerRating(@RequestBody RatingDeleteDto ratingDeleteDto, Principal principal, HttpServletResponse response) throws IOException {
        ratingDeleteDto.setUsername(principal.getName());
        ratingService.deleteUserRating(ratingDeleteDto);
        response.sendRedirect("/");
    }

    @GetMapping(value = "/beers/{beerName}")
    public ModelAndView showBeer(@PathVariable String beerName, Principal principal) {
        String username = principal != null ? principal.getName() : "";
        ModelAndView mav = new ModelAndView("singleBeer");
        try {
            Beer beer = beersService.getByName(beerName);
            BeerRatingDto beerRatingDto = beersService.getById(beer.getId(), username);
            mav.addObject("beer", beerRatingDto);
        } catch (EntityNotFoundException e) {
            return new ModelAndView("redirect:/access-denied");
        }

        return mav;
    }


    @GetMapping("/beers/update/{beerName}")
    public ModelAndView showUpdateBeerForm(@PathVariable String beerName) {
        Beer beer = beersService.getByName(beerName);
        List<Style> styles = styleService.getAll();
        List<Brewery> breweries = breweryService.getAll();
        List<Country> countries = countryService.getAll();

        ModelAndView mav = new ModelAndView("updateBeer");
        mav.addObject("beer", mapper.toBeerDto(beer));
        mav.addObject("styles", styles);
        mav.addObject("breweries", breweries);
        mav.addObject("countries", countries);
        return mav;
    }

    @PostMapping("/beers/update")
    public String updateBeer(@Valid @ModelAttribute BeerDto beerDto) {
        beersService.updateBeer(beerDto);
        return String.format("redirect:/beers/%s", beerDto.getName());
    }

    @PostMapping("/beers/addToWishList/{beerName}")
    public String addToWishList(@PathVariable String beerName, Principal principal) {
        Beer beer = beersService.getByName(beerName);
        beersService.addToWishList(principal.getName(), beer.getId());

        return "redirect:/profile";
    }

    @PostMapping("/beers/addToDrankList/{beerName}")
    public String addToDrankList(@PathVariable String beerName, Principal principal) {
        Beer beer = beersService.getByName(beerName);
        beersService.addToDrankList(principal.getName(), beer.getId());

        return "redirect:/profile";
    }


    @GetMapping("/admin/beers")
    public ModelAndView showBreweries() {
        ModelAndView mav = new ModelAndView("beers");
        mav.addObject("beers", beersService.getAll());
        return mav;
    }

    @PostMapping("/admin/beers/delete/{beerName}")
    public String deleteBrewery(@PathVariable String beerName) {
        beersService.deleteBeer(beersService.getByName(beerName));

        return "redirect:/admin/beers";
    }


    private ModelAndView createBeerMav(ModelAndView mav) {
        List<Style> styles = styleService.getAll();
        List<Brewery> breweries = breweryService.getAll();
        List<Country> countries = countryService.getAll();
        Set<Tag> tags = tagService.getAll();

        mav.addObject("beer", new BeerDto());
        mav.addObject("styles", styles);
        mav.addObject("breweries", breweries);
        mav.addObject("countries", countries);
        mav.addObject("tags", tags);

        return mav;
    }

    private ModelAndView getModelAndView(ModelAndView mav, List<BeerRatingDto> beers) {
        mav.addObject("beers", beers);

        List<Style> styles = styleService.getAll();
        List<Brewery> breweries = breweryService.getAll();
        List<Country> countries = countryService.getAll();
        Set<Tag> tags = tagService.getAll();

        mav.addObject("beerSearch", new BeerSortDto());
        mav.addObject("styles", styles);
        mav.addObject("breweries", breweries);
        mav.addObject("countries", countries);
        mav.addObject("tags", tags);
        return mav;
    }

}


