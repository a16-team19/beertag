package com.teamwork.beertag.models.dtos;

public class RatingDeleteDto {

    private String username;

    private String beerName;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBeerName() {
        return beerName;
    }

    public void setBeerName(String beerName) {
        this.beerName = beerName;
    }
}
