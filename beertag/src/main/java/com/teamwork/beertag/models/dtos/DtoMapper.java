package com.teamwork.beertag.models.dtos;

import com.teamwork.beertag.models.*;
import com.teamwork.beertag.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DtoMapper {
    private final StyleService styleService;
    private final BreweryService breweryService;
    private final CountryService countryService;
    private final RatingService ratingService;
    private final TagService tagService;


    @Autowired
    public DtoMapper(StyleService styleService, BreweryService breweryService, CountryService countryService, RatingService ratingService, TagService tagService) {
        this.styleService = styleService;
        this.breweryService = breweryService;
        this.countryService = countryService;
        this.ratingService = ratingService;
        this.tagService = tagService;
    }

    public Beer fromBeerDto(BeerDto beerDto) {
        Beer beer = new Beer(beerDto.getName(), beerDto.getAbv());
        Style style = styleService.getById(beerDto.getStyleId());
        Brewery brewery = breweryService.getById(beerDto.getBreweryId());
        Country country = countryService.getById(beerDto.getCountryId());
        beer.setDescription(beerDto.getDescription());
        beer.setStyle(style);
        beer.setBrewery(brewery);
        beer.setCountry(country);
        return beer;
    }

    public BeerRatingDto fromBeerToRatingDto(Beer beer) {
        BeerRatingDto beerRatingDto = new BeerRatingDto();
        beerRatingDto.setName(beer.getName());
        beerRatingDto.setAbv(beer.getAbv());
        beerRatingDto.setStyle(beer.getStyle());
        beerRatingDto.setBrewery(beer.getBrewery());
        beerRatingDto.setCountry(beer.getCountry());
        beerRatingDto.setDescription(beer.getDescription());
        beerRatingDto.setUser(beer.getCreatedBy());
        beerRatingDto.setPictureUrl(beer.getPictureUrl());
        beerRatingDto.setTags(beer.getTags());
        beerRatingDto.setNumberOfRatings(ratingService.getNumberOfRatings(beer.getId()));
        beerRatingDto.setRating(ratingService.getRating(beer.getId()));
        return beerRatingDto;
    }

    public User fromUserRegisterDto(UserRegisterDto userRegisterDto) {
        return new User(userRegisterDto.getUsername(), userRegisterDto.getPassword());
    }

    public UserDetails fromUserDetailsDto(UserDetailsDto userDetailsDto) {
        UserDetails userDetails = new UserDetails();
        userDetails.setFirstName(userDetailsDto.getFirstName());
        userDetails.setLastName(userDetailsDto.getLastName());
        userDetails.setEmail(userDetailsDto.getEmail());
        userDetails.setGender(userDetailsDto.getGender());
        userDetails.setAge(userDetailsDto.getAge());

        return userDetails;
    }

    public UserDetailsDto toUserDetailsDto(UserDetails userDetails) {
        UserDetailsDto userDetailsDto = new UserDetailsDto();
        userDetailsDto.setFirstName(userDetails.getFirstName());
        userDetailsDto.setLastName(userDetails.getLastName());
        userDetailsDto.setEmail(userDetails.getEmail());
        if (userDetails.getAge() != null) {
            userDetailsDto.setAge(userDetails.getAge());
        }
        userDetailsDto.setGender(userDetails.getGender());

        return userDetailsDto;
    }

    public BeerDto toBeerDto(Beer beer) {
        BeerDto beerDto = new BeerDto();
        beerDto.setName(beer.getName());
        beerDto.setAbv(beer.getAbv());
        beerDto.setDescription(beer.getDescription());
        beerDto.setBreweryId(beer.getBrewery().getId());
        beerDto.setCountryId(beer.getCountry().getId());
        beerDto.setStyleId(beer.getStyle().getId());

        return beerDto;
    }

    public BeerSortObjDto getSortBeer(BeerSortDto beerSortDto) {
        BeerSortObjDto finalBeerSort = new BeerSortObjDto();
        if (beerSortDto.getStyleId() != 0) {
            Style style = styleService.getById(beerSortDto.getStyleId());
            finalBeerSort.setStyle(style);
        }
        if (beerSortDto.getBreweryId() != 0) {
            Brewery brewery = breweryService.getById(beerSortDto.getBreweryId());
            finalBeerSort.setBrewery(brewery);
        }
        if (beerSortDto.getTagId() != 0) {
            Tag tag = tagService.getTagById(beerSortDto.getTagId());
            finalBeerSort.setTag(tag);
        }
        if (!beerSortDto.getCountryId().equals("0")) {
            Country country = countryService.getById(beerSortDto.getCountryId());
            finalBeerSort.setCountry(country);
        }

        finalBeerSort.setName(beerSortDto.getName());
        finalBeerSort.setAbv(beerSortDto.getAbv());
        return finalBeerSort;
    }

}

