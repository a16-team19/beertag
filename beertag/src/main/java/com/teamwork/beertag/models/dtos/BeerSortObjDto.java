package com.teamwork.beertag.models.dtos;

import com.teamwork.beertag.models.Brewery;
import com.teamwork.beertag.models.Country;
import com.teamwork.beertag.models.Style;
import com.teamwork.beertag.models.Tag;

public class BeerSortObjDto {

    private String name;
    private Style style;
    private Brewery brewery;
    private Country country;
    private Tag tag;
    private Integer abv;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public Integer getAbv() {
        return abv;
    }

    public void setAbv(Integer abv) {
        this.abv = abv;
    }
}
