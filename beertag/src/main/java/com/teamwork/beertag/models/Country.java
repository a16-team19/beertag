package com.teamwork.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "countries")
public class Country {

    @Id
    @NotNull
    @Size(min = 1, max = 5, message = "Id has to be between 1 and 5 letters!")
    @Column(name = "country_id")
    private String id;

    @NotNull
    @Size(min = 2, max = 16, message = "Name has to be between 2 and 16 letters!")
    @Column(name = "name")
    private String name;

    @Column(name = "enabled")
    private int enabled;

    public Country() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }
}
