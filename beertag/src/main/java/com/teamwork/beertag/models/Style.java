package com.teamwork.beertag.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "styles")
public class Style {

    @Id
    @NotNull
    @Column(name = "style_id")
    private int id;

    @NotNull
    @Size(min = 1)
    @Size(min = 2, max = 16, message = "Name has to be between 2 and 16 letters!")
    private String name;

    @Column(name = "enabled")
    private int enabled;

    public Style() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }
}
