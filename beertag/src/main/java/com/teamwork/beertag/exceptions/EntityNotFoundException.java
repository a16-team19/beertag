package com.teamwork.beertag.exceptions;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String itemType, String id) {
        this(itemType,"id",id);
    }

    public EntityNotFoundException(String itemType, String attribute, String value){
        super(String.format("%s with name %s %s not found", itemType,attribute,value));
    }

    public EntityNotFoundException(String format) {
        super(format);
    }
}
