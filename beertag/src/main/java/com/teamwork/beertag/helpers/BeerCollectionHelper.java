package com.teamwork.beertag.helpers;

import com.teamwork.beertag.models.Beer;

import java.util.List;
import java.util.stream.Collectors;

public class BeerCollectionHelper {
    public static List<Beer> filterByName(List<Beer> beers, String name){
        return beers.stream().
                filter(beer -> beer.getName().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }

    public static List<Beer> filterByStyle(List<Beer> beers, String style){
        return beers.stream().
                filter(beer -> beer.getStyle().getName().toLowerCase().contains(style.toLowerCase()))
                .collect(Collectors.toList());
    }

    public static List<Beer> filterByBrewery(List<Beer> beers,String brewery){
        return beers.stream()
                .filter(beer -> beer.getBrewery().getName().toLowerCase().contains(brewery.toLowerCase()))
                .collect(Collectors.toList());
    }

    public static List<Beer> filterByCountry(List<Beer> beers,String country){
        return beers.stream()
                .filter(beer -> beer.getCountry().getName().toLowerCase().contains(country.toLowerCase()))
                .collect(Collectors.toList());
    }

    public static List<Beer> filterByAbv(List<Beer> beers,double abv){
        return beers.stream()
                .filter(beer -> beer.getAbv()==abv)
                .collect(Collectors.toList());
    }
}

