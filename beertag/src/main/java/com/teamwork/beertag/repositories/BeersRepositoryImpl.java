package com.teamwork.beertag.repositories;

import com.teamwork.beertag.exceptions.*;
import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.Tag;
import com.teamwork.beertag.models.dtos.*;
import com.teamwork.beertag.repositories.contracts.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
@Repository
@PropertySource("classpath:application.properties")
public class BeersRepositoryImpl implements BeersRepository {

    public static final String DOESNT_EXIST_ID = "%s with id = %d does not exist";
    public static final String DOESNT_EXIST_NAME = "%s with name = %s does not exist";

    private final String dbUrl;
    private final String dbUserName;
    private final String dbPassword;
    private final SessionFactory factory;

    @Autowired
    public BeersRepositoryImpl(SessionFactory factory, Environment env) {
        this.factory = factory;
        dbUrl = env.getProperty("database.url");
        dbUserName = env.getProperty("database.username");
        dbPassword = env.getProperty("database.password");
    }

    @Override
    public List<Beer> getAll() {
        try (Session session = factory.openSession()) {
            return session.createQuery("from Beer ", Beer.class)
                    .list();
        }
    }

    @Override
    public List<Beer> getTop4Beers() {
        List<Beer> beers = new ArrayList<>();
        String query = "select b.beer_id, AVG(r.rating)  rating\n" +
                "from beers b\n" +
                "   left join beers_ratings r on r.beer_id = b.beer_id\n" +
                "    group by b.beer_id\n" +
                "    order by rating desc\n" +
                "    limit 4;\n" +
                ";";

        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(query)) {
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    Beer beer = getById(result.getInt("b.beer_id"));
                    beers.add(beer);
                }
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return beers;
    }

    @Override
    public Beer getByName(String name) {
        try (Session session = factory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name = :name");
            query.setParameter("name", name);
            if (query.getResultList().isEmpty()) {
                throw new EntityNotFoundException(String.format(DOESNT_EXIST_NAME, "Beer", name));
            }
            return query.getResultList().get(0);
        }
    }

    @Override
    public boolean checkBeerExists(String name) {
        try (Session session = factory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name = :name");
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    @Override
    public Beer createBeer(Beer beer) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(beer);
            session.getTransaction().commit();
            return beer;
        }
    }

    public List<Beer> getWishOrDrankList(int userId, String status) {
        List<Beer> beers = new ArrayList<>();
        String query = "select beer_id \n" +
                "from beers_users bu \n" +
                "where bu.status = ? \n" +
                " and bu.user_id = ? ;";

        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, status);
            statement.setInt(2, userId);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    Beer beer = getById(result.getInt("beer_id"));
                    beers.add(beer);
                }
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return beers;
    }

    @Override
    public boolean checkBeerExistInWishDrankList(String status, int beerId, int userId) {
        List<Integer> beersId = new ArrayList<>();
        String query = "select beer_id \n" +
                "from beers_users bu \n" +
                "where bu.status = ? \n" +
                "  and bu.beer_id = ? \n" +
                "  and bu.user_id = ? ;";

        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, status);
            statement.setInt(2, beerId);
            statement.setInt(3, userId);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    int beer = result.getInt("beer_id");
                    beersId.add(beer);
                }
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return !beersId.isEmpty();
    }

    @Override
    public void addToWishDrankList(String status, int beerId, int userId) {
        String query = "insert into beers_users(status,beer_id,user_id) values (?,?,?)";
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, status);
            statement.setInt(2, beerId);
            statement.setInt(3, userId);
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void removeFromWishDrankList(int beerId, int userId) {
        String query = "delete from beers_users " +
                "where beer_id = ? " +
                "and user_id =? ";
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, beerId);
            statement.setInt(2, userId);
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }


    @Override
    public Beer getById(int id) {
        try (Session session = factory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            if (beer == null) {
                throw new EntityNotFoundException(String.format(DOESNT_EXIST_ID, "Beer", id));
            }
            return beer;
        }
    }

    @Override
    public void updateBeer(Beer beer) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(beer);
            session.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void deleteBeer(Beer beer) {
        String queryList = "delete from beers_users where beer_id = ?";
        String queryRating = "delete from beers_ratings where beer_id = ?";
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(queryList)) {
            statement.setInt(1, beer.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(queryRating)) {
            statement.setInt(1, beer.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(beer);
            session.getTransaction().commit();
        }
    }


    @Override
    public List<Beer> filterBeersByName(String beerName) {
        try (Session session = factory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Beer> query = builder.createQuery(Beer.class);
            Root<Beer> root = query.from(Beer.class);
            query.select(root);

            List<Predicate> criteria = new ArrayList<>();

            if (beerName != null && !beerName.equals("")) {
                criteria.add(builder.like(root.get("name"), "%" + beerName + "%"));
            }

            return getBeers(builder, query, criteria);
        }
    }

    @Override
    public List<Beer> filterBeers(BeerSortObjDto beerSort) {

        try (Session session = factory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Beer> query = builder.createQuery(Beer.class);
            Root<Beer> root = query.from(Beer.class);
            query.select(root);

            List<Predicate> criteria = new ArrayList<>();

            if (beerSort.getName() != null && !beerSort.getName().equals("")) {
                criteria.add(builder.like(root.get("name"), "%" + beerSort.getName() + "%"));
            }
            if (beerSort.getStyle() != null) {
                criteria.add(builder.equal(root.get("style"), beerSort.getStyle()));
            }
            if (beerSort.getCountry() != null) {
                criteria.add(builder.equal(root.get("country"), beerSort.getCountry()));
            }
            if (beerSort.getBrewery() != null) {
                criteria.add(builder.equal(root.get("brewery"), beerSort.getBrewery()));
            }

            if (beerSort.getTag() != null) {
                criteria.add(builder.isMember(beerSort.getTag(),root.get("tags")));
            }

            if (beerSort.getAbv() != null){
                criteria.add(builder.ge(root.get("abv"), beerSort.getAbv()));
            }

            return getBeers(builder, query, criteria);
        }
    }

    private List<Beer> getBeers(CriteriaBuilder builder, CriteriaQuery<Beer> query, List<Predicate> criteria) {
        if (criteria.size() == 0) {
            return getAll();
        } else if (criteria.size() == 1) {
            query.where(criteria.get(0));
        } else {
            query.where(builder.and(criteria.toArray(new Predicate[0])));
        }
        TypedQuery<Beer> resultQuery = factory.getCurrentSession().createQuery(query);
        return resultQuery.getResultList();
    }
}

