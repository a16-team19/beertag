package com.teamwork.beertag.repositories;

import com.teamwork.beertag.exceptions.DuplicateEntityException;
import com.teamwork.beertag.repositories.contracts.RatingRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;

@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
@Repository
@PropertySource("classpath:application.properties")
public class RatingRepositoryImpl implements RatingRepository {


    private final String dbUrl;
    private final String dbUserName;
    private final String dbPassword;
    private final SessionFactory factory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory factory, Environment env) {
        this.factory = factory;
        dbUrl = env.getProperty("database.url");
        dbUserName = env.getProperty("database.username");
        dbPassword = env.getProperty("database.password");
    }

    @Override
    public Double getRating(int beerId) {
        String query = "select avg(rating) as rating from beers_ratings where beer_id = ?";
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, beerId);
            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    return result.getDouble("rating");
                }
                return 0.0;
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Integer getNumberOfRatings(int beerId) {
        String query = "select count(rating) as n_ratings from beers_ratings where beer_id = ?";
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, beerId);
            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    return result.getInt("n_ratings");
                }
                return 0;
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }


    @Override
    public void addRating(int beerId, int userId, int rating) {
        String query = "INSERT INTO beers_ratings(rating, beer_id, user_id) VALUES (?,?,?)";
        if (getRatingFromUser(beerId, userId) != 0){
            throw new DuplicateEntityException("This user already rated this beer!");
        }
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, rating);
            statement.setInt(2, beerId);
            statement.setInt(3, userId);
            statement.executeUpdate();

        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Integer getRatingFromUser(int beerId, int userId) {
        String query = "select rating as userRating from beers_ratings where beer_id = ? and user_id = ?";
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, beerId);
            statement.setInt(2, userId);
            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    return result.getInt("userRating");
                }
                return 0;
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }


    @Override
    public void deleteUserRating(int beerId, int userId) {
        String query = "delete from beers_ratings where beer_id = ? and user_id = ?";
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, beerId);
            statement.setInt(2, userId);
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
}
