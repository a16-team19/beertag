package com.teamwork.beertag.repositories.contracts;

import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.dtos.BeerSortDto;
import com.teamwork.beertag.models.dtos.BeerSortObjDto;

import java.util.List;

public interface BeersRepository {

    Beer createBeer(Beer beer);

    Beer getById(int id);

    Beer getByName(String name);

    boolean checkBeerExists(String name);

    void updateBeer(Beer beer);

    void deleteBeer(Beer beer);

    List<Beer> getWishOrDrankList(int userId, String status);

    List<Beer> filterBeersByName(String beerName);

    List<Beer> filterBeers(BeerSortObjDto beerSort);

    List<Beer> getAll();

    List<Beer> getTop4Beers();

    void addToWishDrankList(String status, int beerId, int userId);

    void removeFromWishDrankList(int beerId, int userId);

    boolean checkBeerExistInWishDrankList(String status, int beerId, int userId);

}
