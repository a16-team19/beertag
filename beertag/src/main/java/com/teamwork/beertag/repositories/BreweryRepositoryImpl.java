package com.teamwork.beertag.repositories;

import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.Brewery;
import com.teamwork.beertag.repositories.contracts.BreweryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
@Repository
public class BreweryRepositoryImpl implements BreweryRepository {

    public static final int DELETED = 0;
    private final SessionFactory factory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public List<Brewery> getAll() {
        try (Session session = factory.openSession()) {
            return session.createQuery("from Brewery where enabled = 1 ", Brewery.class)
                    .list();
        }
    }

    @Override
    public Brewery getById(int id) {
        try (Session session = factory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            if (brewery == null) {
                throw new EntityNotFoundException(String.format("Brewery with id = %d does not exist", id));
            }
            return brewery;
        }
    }

    @Override
    public Brewery createBrewery(Brewery brewery) {
        brewery.setEnabled(1);
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(brewery);
            session.getTransaction().commit();
        } catch (RuntimeException ex) {
            System.out.println(ex);
        }
        return brewery;
    }

    @Override
    public void deleteBrewery(Brewery brewery) {
        brewery.setEnabled(DELETED);
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(brewery);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateBrewery(Brewery brewery) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(brewery);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean checkBreweryExist(String name) {
        try (Session session = factory.openSession()) {
            Query<Beer> query = session.createQuery("from Brewery where name = :name");
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }
}
