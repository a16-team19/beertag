package com.teamwork.beertag.repositories;

import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.Country;
import com.teamwork.beertag.repositories.contracts.CountryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
@Repository
public class CountryRepositoryImpl implements CountryRepository {

    public static final int DELETED = 0;
    private final SessionFactory factory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public List<Country> getAll() {
        try (Session session = factory.openSession()) {
            return session.createQuery("from Country where enabled = 1", Country.class)
                    .list();
        }

    }

    @Override
    public Country getById(String id) {
        try (Session session = factory.openSession()) {
            Country country = session.get(Country.class, id);
            if (country == null) {
                throw new EntityNotFoundException(String.format("Country with id = %s does not exist", id));
            }
            return country;
        }

    }

    @Override
    public void createCountry(Country country) {
        country.setEnabled(1);
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(country);
            session.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void deleteCountry(Country country) {
        country.setEnabled(DELETED);
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(country);
            session.getTransaction().commit();
        }
    }

    @Override
    public void updateCountry(Country country) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(country);
            session.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public boolean checkCountryExist(String name) {
        try (Session session = factory.openSession()) {
            Query<Beer> query = session.createQuery("from Country where name = :name");
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }
}
