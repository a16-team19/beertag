package com.teamwork.beertag.repositories;

import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.User;
import com.teamwork.beertag.models.UserDetails;
import com.teamwork.beertag.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory factory;

    @Autowired
    public UserRepositoryImpl(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = factory.openSession()) {
            return session.createQuery("from User where enabled = 1 ", User.class).list();
        }
    }

    @Override
    public User getUserById(int id) {
        try (Session session = factory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException(String.format("User with id = %d does not exist", id));
            }
            return user;
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = factory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username");
            query.setParameter("username", username);
            User user = query.getSingleResult();
            if (user == null) {
                throw new EntityNotFoundException(String.format("User with username = %s does not exist", username));
            }
            return user;
        }
    }

    @Override
    public void updateUser(User user) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void deleteUser(User user) {
        user.setEnabled(0);
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public User createUser(User user) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            return user;
        }
    }

    @Override
    public boolean checkUserExist(String name) {
        try (Session session = factory.openSession()) {
            Query<User> query = session.createQuery("from User where userName = :name");
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    @Override
    public void updateUserDetails(UserDetails userDetails) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(userDetails);
            session.getTransaction().commit();
        }
    }

    @Override
    public void createUserDetails(int userId, UserDetails userDetails) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(userDetails);
            session.getTransaction().commit();
        }
        User user = getUserById(userId);
        user.setUserDetails(userDetails);
        updateUser(user);
    }


}
