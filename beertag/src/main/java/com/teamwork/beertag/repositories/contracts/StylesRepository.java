package com.teamwork.beertag.repositories.contracts;

import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.Style;

import java.util.List;

public interface StylesRepository {
    List<Style> getAll();

    Style getById(int id);

    boolean checkStyleExists(String name);

    Style createStyle(Style style);

    void updateStyle(Style style);

    void deleteStyle(Style style);

    List<Beer> getByStyle(int styleId);
}
