package com.teamwork.beertag.repositories.contracts;

public interface RatingRepository {

    void addRating(int beerId, int userId, int rating);

    Double getRating(int beerId);

    Integer getNumberOfRatings(int beerId);

    Integer getRatingFromUser(int beerId, int userId);

    void deleteUserRating(int beerId, int userId);
}
