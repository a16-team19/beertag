package com.teamwork.beertag.repositories.contracts;

import com.teamwork.beertag.models.Tag;

import java.util.Set;

public interface TagRepository {

    void addTag(int beerId, int tagId);

    Tag getTagById(int id);

    Set<Tag> getAll();

    void deleteTag(Tag tag);

    void createTag(Tag tag);

    boolean checkTagExist(String name);
}
