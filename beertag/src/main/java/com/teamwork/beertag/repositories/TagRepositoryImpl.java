package com.teamwork.beertag.repositories;

import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.Tag;
import com.teamwork.beertag.repositories.contracts.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
@Repository
@PropertySource("classpath:application.properties")
public class TagRepositoryImpl implements TagRepository {

    private final String dbUrl;
    private final String dbUserName;
    private final String dbPassword;
    private final SessionFactory factory;

    @Autowired
    public TagRepositoryImpl(SessionFactory factory, Environment env) {
        this.factory = factory;
        dbUrl = env.getProperty("database.url");
        dbUserName = env.getProperty("database.username");
        dbPassword = env.getProperty("database.password");
    }


    @Override
    public void addTag(int beerId, int tagId) {
        String query = "INSERT INTO beer_tags(beer_id, tag_id) VALUES (?,?)";
        try (Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, beerId);
            statement.setInt(2, tagId);
            statement.executeUpdate();

        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Tag getTagById(int id) {
        try (Session session = factory.openSession()) {
            return session.get(Tag.class, id);
        }
    }


    @Override
    public Set<Tag> getAll() {
        try (Session session = factory.openSession()) {
            List<Tag> tagsList = session.createQuery("from Tag where enabled = 1", Tag.class)
                    .list();
            return new HashSet<>(tagsList);
        }
    }

    @Override
    public void deleteTag(Tag tag) {
        tag.setEnabled(0);
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void createTag(Tag tag) {
        tag.setEnabled(1);
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean checkTagExist(String name) {
        try (Session session = factory.openSession()) {
            Query query = session.createQuery("from Tag where name = :name");
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }
}
