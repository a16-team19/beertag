package com.teamwork.beertag.repositories;

import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.Style;
import com.teamwork.beertag.repositories.contracts.StylesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true, propagation = Propagation.NOT_SUPPORTED)
@Repository
public class StylesRepositoryImpl implements StylesRepository {

    private final SessionFactory factory;

    @Autowired
    public StylesRepositoryImpl(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public List<Style> getAll() {
        try (Session session = factory.openSession()) {
            return session.createQuery("from Style where enabled = 1", Style.class).list();
        }
    }

    @Override
    public Style getById(int id) {
        try (Session session = factory.openSession()) {
            Style style = session.get(Style.class, id);
            if (style == null) {
                throw new EntityNotFoundException(String.format("Style with id = %d does not exist", id));
            }
            return style;
        }
    }

    @Override
    public Style createStyle(Style style) {
        style.setEnabled(1);
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(style);
            session.getTransaction().commit();
            return style;
        }
    }

    @Override
    public boolean checkStyleExists(String name) {
        try (Session session = factory.openSession()) {
            Query query = session.createQuery("from Style where name = :name");
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    @Override
    public void updateStyle(Style style) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void deleteStyle(Style style) {
        style.setEnabled(0);
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public List<Beer> getByStyle(int styleId) {
        try (Session session = factory.openSession()) {
            getById(styleId);
            Query<Beer> query = session.createQuery("from Beer where style.id = :styleId", Beer.class);
            query.setParameter("styleId", styleId);
            return query.list();
        }
    }
}
