package com.teamwork.beertag.repositories.contracts;

import com.teamwork.beertag.models.User;
import com.teamwork.beertag.models.UserDetails;

import java.util.List;

public interface UserRepository {

    List<User> getAll();

    User getUserById(int id);

    User getUserByUsername(String username);

    void updateUser(User user);

    void deleteUser(User user);

    User createUser(User user);

    boolean checkUserExist(String name);

    void updateUserDetails(UserDetails userDetails);

    void createUserDetails(int userId, UserDetails userDetails);
}
