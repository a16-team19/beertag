package com.teamwork.beertag.services.contracts;

import com.teamwork.beertag.models.dtos.RatingDeleteDto;
import com.teamwork.beertag.models.dtos.RatingDto;

public interface RatingService {

    void addRating(RatingDto ratingDto);

    void deleteUserRating(RatingDeleteDto ratingDeleteDto);

    Double getRating(int beerId);

    Integer getNumberOfRatings(int beerId);

    Integer getRatingFromUser(int beerId, int userId);
}
