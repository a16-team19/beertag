package com.teamwork.beertag.services;

import com.teamwork.beertag.models.Tag;
import com.teamwork.beertag.repositories.contracts.TagRepository;
import com.teamwork.beertag.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public void addTag(int beerId, int tagId) {
        tagRepository.addTag(beerId, tagId);
    }

    @Override
    public Tag getTagById(Integer id) {
        return tagRepository.getTagById(id);
    }

    @Override
    public Set<Tag> getAll() {
        return tagRepository.getAll();
    }

    @Override
    public void deleteTag(Tag tag) {
        tagRepository.deleteTag(tag);
    }

    @Override
    public void createTag(Tag tag) {
        tagRepository.createTag(tag);
    }

    @Override
    public boolean checkTagExist(String name) {
        return tagRepository.checkTagExist(name);
    }
}
