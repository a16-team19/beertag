package com.teamwork.beertag.services.contracts;

import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.dtos.BeerDto;
import com.teamwork.beertag.models.dtos.BeerSortDto;
import com.teamwork.beertag.models.dtos.BeerRatingDto;

import java.util.List;

public interface BeersService {

    List<BeerRatingDto> getWishList(String username);

    List<BeerRatingDto> getDrankList(String username);

    List<BeerRatingDto> getTop4Beers(String username);

    List<BeerRatingDto> filterBeersByName(String beerName, String username);

    List<BeerRatingDto> getAll();

    List<BeerRatingDto> filterBeers(BeerSortDto beerSortDto, String username);

    BeerRatingDto getById(Integer id, String username);

    Beer getByName(String name);

    Beer createBeer(BeerDto beerDto);

    void addToWishList(String username, int beerId);

    void addToDrankList(String username, int beerId);

    void updateBeer(BeerDto beerDto);

    void deleteBeer(Beer beer);
}
