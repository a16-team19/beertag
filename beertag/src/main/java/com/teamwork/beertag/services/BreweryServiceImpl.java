package com.teamwork.beertag.services;

import com.teamwork.beertag.models.Brewery;
import com.teamwork.beertag.repositories.contracts.BreweryRepository;
import com.teamwork.beertag.services.contracts.BreweryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreweryServiceImpl implements BreweryService {

    private final BreweryRepository breweryRepository;

    public BreweryServiceImpl(BreweryRepository breweryRepository) {
        this.breweryRepository = breweryRepository;
    }

    @Override
    public List<Brewery> getAll() {
        return breweryRepository.getAll();
    }

    @Override
    public Brewery getById(int id) {
        return breweryRepository.getById(id);
    }

    @Override
    public Brewery createBrewery(Brewery brewery) {
        return breweryRepository.createBrewery(brewery);
    }

    @Override
    public void deleteBrewery(Brewery brewery) {
        breweryRepository.deleteBrewery(brewery);
    }

    @Override
    public void updateBrewery(Brewery brewery) {
        breweryRepository.updateBrewery(brewery);
    }

    @Override
    public boolean checkBreweryExist(String name) {
        return breweryRepository.checkBreweryExist(name);
    }
}
