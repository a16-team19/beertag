package com.teamwork.beertag.services.contracts;

import com.teamwork.beertag.models.User;
import com.teamwork.beertag.models.UserDetails;
import com.teamwork.beertag.models.dtos.UserDetailsDto;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getUserById(int id);

    User getUserByUsername(String username);

    void updateUserById(User user);

    void deleteUser(User user);

    User createUser(User user);

    void updateUserDetails(String username, UserDetailsDto userDetailsDto);

    void addDefaultUserDetails(String username);
}
