package com.teamwork.beertag.services.contracts;

import com.teamwork.beertag.models.Brewery;

import java.util.List;

public interface BreweryService {
    List<Brewery> getAll();

    Brewery getById(int id);

    Brewery createBrewery(Brewery brewery);

    void deleteBrewery(Brewery brewery);

    void updateBrewery(Brewery brewery);

    boolean checkBreweryExist(String name);
}
