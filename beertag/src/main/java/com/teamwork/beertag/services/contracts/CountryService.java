package com.teamwork.beertag.services.contracts;

import com.teamwork.beertag.models.Country;

import java.util.List;

public interface CountryService {

    List<Country> getAll();

    Country getById(String id);

    void createCountry(Country country);

    void deleteCountry(Country country);

    void updateCountry(Country country);

    boolean checkCountryExist(String name);
}
