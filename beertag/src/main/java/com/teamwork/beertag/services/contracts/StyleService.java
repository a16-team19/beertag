package com.teamwork.beertag.services.contracts;

import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.Style;

import java.util.List;

public interface StyleService {
    List<Style> getAll();

    Style getById(int id);

    Style createStyle(Style style);

    void updateStyle(Style style);

    void deleteStyle(Style style);

    List<Beer> getByStyle(int styleId);

    boolean checkStyleExist(String name);
}

