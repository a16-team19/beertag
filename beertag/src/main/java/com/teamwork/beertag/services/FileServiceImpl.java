package com.teamwork.beertag.services;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import com.teamwork.beertag.exceptions.FileStorageException;
import com.teamwork.beertag.services.contracts.FileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileServiceImpl implements FileService {

    @Value("src/main/webapp/WEB-INF/images/")
    public String uploadDir;


    @Override
    public String uploadFile(MultipartFile file, String name) {
        String[] contents = file.getContentType().split("/");
        String content = contents[1];
        try {
            Path copyLocation = Paths
                    .get(uploadDir + File.separator + StringUtils.cleanPath(name + "." + content));
            Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileStorageException("Could not store file " + file.getOriginalFilename()
                    + ". Please try again!");
        }

        return "/images/" + name + "." + content;
    }
}
