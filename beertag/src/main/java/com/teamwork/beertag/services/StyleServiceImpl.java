package com.teamwork.beertag.services;


import com.teamwork.beertag.exceptions.DuplicateEntityException;
import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.Style;
import com.teamwork.beertag.repositories.contracts.StylesRepository;
import com.teamwork.beertag.services.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {

    private final StylesRepository stylesRepository;

    @Autowired
    public StyleServiceImpl(StylesRepository stylesRepository) {
        this.stylesRepository = stylesRepository;
    }

    @Override
    public List<Style> getAll() {
        return stylesRepository.getAll();
    }

    @Override
    public Style getById(int id) {
        return stylesRepository.getById(id);
    }

    @Override
    public Style createStyle(Style style) {
        if (stylesRepository.checkStyleExists(style.getName())) {
            throw new DuplicateEntityException(
                    String.format("Style with name %s already exists", style.getName())
            );
        }
        return stylesRepository.createStyle(style);
    }

    @Override
    public void updateStyle(Style style) {
        stylesRepository.updateStyle(style);
    }

    @Override
    public void deleteStyle(Style style) {
        stylesRepository.deleteStyle(style);
    }

    @Override
    public List<Beer> getByStyle(int styleId) {
        return stylesRepository.getByStyle(styleId);
    }

    @Override
    public boolean checkStyleExist(String name) {
        return stylesRepository.checkStyleExists(name);
    }
}

