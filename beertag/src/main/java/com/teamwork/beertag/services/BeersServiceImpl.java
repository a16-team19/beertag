package com.teamwork.beertag.services;

import com.teamwork.beertag.exceptions.DuplicateEntityException;
import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.Tag;
import com.teamwork.beertag.models.User;
import com.teamwork.beertag.models.dtos.*;
import com.teamwork.beertag.repositories.contracts.BeersRepository;
import com.teamwork.beertag.repositories.contracts.RatingRepository;
import com.teamwork.beertag.services.contracts.BeersService;
import com.teamwork.beertag.services.contracts.TagService;
import com.teamwork.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BeersServiceImpl implements BeersService {

    public static final String DUPLICATE_NAME = "Beer with name %s already exists";
    public static final String IMAGE_PATH_FOR_BEER = "beers/";
    public static final String DEFAULT_BEER_IMAGE = "/images/beers/default-beer.jpg";
    public static final String WISH_LIST_STATUS = "wish";
    public static final String DRANK_LIST_STATUS = "drank";

    private final BeersRepository beersRepository;
    private final RatingRepository ratingService;
    private final UserService userService;
    private final TagService tagService;
    private final DtoMapper mapper;
    private final FileServiceImpl fileService;

    @Autowired
    public BeersServiceImpl(BeersRepository beersRepository, RatingRepository ratingService, UserService userService,
                            TagService tagService, DtoMapper mapper, FileServiceImpl fileService) {
        this.beersRepository = beersRepository;
        this.ratingService = ratingService;
        this.userService = userService;
        this.tagService = tagService;
        this.mapper = mapper;
        this.fileService = fileService;
    }

    @Override
    public Beer createBeer(BeerDto beerDto) {
        if (beersRepository.checkBeerExists(beerDto.getName())) {
            throw new DuplicateEntityException(
                    String.format(DUPLICATE_NAME, beerDto.getName())
            );
        }
        Beer beer = mapper.fromBeerDto(beerDto);
        Set<Tag> tagSet = new HashSet<>();
        for (int i = 0; i < beerDto.getTags().size(); i++) {
            tagSet.add(tagService.getTagById(Integer.parseInt(beerDto.getTags().get(i))));
        }

        beer.setTags(tagSet);
        beer.setCreatedBy(userService.getUserByUsername(beerDto.getCreatedBy()));
        beer.setPictureUrl(getPictureUrl(beerDto));
        beersRepository.createBeer(beer);
        return beer;
    }

    @Override
    public void addToWishList(String username, int beerId) {
        User user = userService.getUserByUsername(username);
        removeIfExistInWishDrankList(DRANK_LIST_STATUS, beerId, user.getId());
        if (beersRepository.checkBeerExistInWishDrankList(DRANK_LIST_STATUS, beerId, user.getId())) {
            return;
        }
        beersRepository.addToWishDrankList(WISH_LIST_STATUS, beerId, user.getId());
    }

    @Override
    public void addToDrankList(String username, int beerId) {
        User user = userService.getUserByUsername(username);
        removeIfExistInWishDrankList(WISH_LIST_STATUS, beerId, user.getId());
        if (beersRepository.checkBeerExistInWishDrankList(DRANK_LIST_STATUS, beerId, user.getId())) {
            return;
        }
        beersRepository.addToWishDrankList(DRANK_LIST_STATUS, beerId, user.getId());
    }


    @Override
    public List<BeerRatingDto> getWishList(String username) {

        return getBeerByWishStatus(username, WISH_LIST_STATUS);
    }

    @Override
    public List<BeerRatingDto> getDrankList(String username) {

        return getBeerByWishStatus(username, DRANK_LIST_STATUS);
    }


    @Override
    public List<BeerRatingDto> getTop4Beers(String username) {

        List<BeerRatingDto> beerRatingDtoList = new ArrayList<>();
        List<Beer> beers = beersRepository.getTop4Beers();

        for (Beer beer : beers) {
            BeerRatingDto beerRatingDto = mapper.fromBeerToRatingDto(beer);
            if (!username.equals("")) {
                User user = userService.getUserByUsername(username);
                beerRatingDto.setCurrentUserRating(ratingService.getRatingFromUser(beer.getId(), user.getId()));
            } else {
                beerRatingDto.setCurrentUserRating(0);
            }
            beerRatingDtoList.add(beerRatingDto);
        }
        return beerRatingDtoList;
    }

    @Override
    public List<BeerRatingDto> getAll() {
        List<Beer> beers = beersRepository.getAll();
        return getBeerRatingDtos(beers, "");
    }

    @Override
    public List<BeerRatingDto> filterBeersByName(String beerName, String username) {
        List<Beer> beers = beersRepository.filterBeersByName(beerName);
        return getBeerRatingDtos(beers, username);
    }

    @Override
    public List<BeerRatingDto> filterBeers(BeerSortDto beerSortDto, String username) {
        List<Beer> beers = beersRepository.filterBeers(mapper.getSortBeer(beerSortDto));
        List<BeerRatingDto> beerRatingDtoList = getBeerRatingDtos(beers, username);
        return beerRatingDtoList
                .stream()
                .filter(beer -> beer.getRating() >= beerSortDto.getRating())
                .collect(Collectors.toList());
    }

    @Override
    public BeerRatingDto getById(Integer id, String username) {
        try {
            Beer beer = beersRepository.getById(id);
            BeerRatingDto beerRatingDto = mapper.fromBeerToRatingDto(beer);
            if (!username.equals("")) {
                User user = userService.getUserByUsername(username);
                beerRatingDto.setCurrentUserRating(ratingService.getRatingFromUser(beer.getId(), user.getId()));
            } else {
                beerRatingDto.setCurrentUserRating(0);
            }
            return beerRatingDto;
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }


    @Override
    public Beer getByName(String name) {
        return beersRepository.getByName(name);
    }


    @Override
    public void updateBeer(BeerDto beerDto) {
        Beer beerUpdate = mapper.fromBeerDto(beerDto);
        Beer beerOld = getByName(beerDto.getName());
        beerOld.setPictureUrl(getPictureUrl(beerDto));
        beerOld.setAbv(beerUpdate.getAbv());
        beerOld.setBrewery(beerUpdate.getBrewery());
        beerOld.setStyle(beerUpdate.getStyle());
        beerOld.setCountry(beerUpdate.getCountry());
        beersRepository.updateBeer(beerOld);
    }

    @Override
    public void deleteBeer(Beer beer) {
        beersRepository.deleteBeer(beer);
    }

    private List<BeerRatingDto> getBeerByWishStatus(String username, String wishStatus) {
        User user = userService.getUserByUsername(username);
        List<BeerRatingDto> beerRatingDtoList = new ArrayList<>();
        List<Beer> beers = beersRepository.getWishOrDrankList(user.getId(), wishStatus);

        for (Beer beer : beers) {
            BeerRatingDto beerRatingDto = mapper.fromBeerToRatingDto(beer);
            beerRatingDtoList.add(beerRatingDto);
        }

        return beerRatingDtoList;
    }

    private List<BeerRatingDto> getBeerRatingDtos(List<Beer> beers, String username) {
        List<BeerRatingDto> beersDto = new ArrayList<>();
        for (Beer beer : beers) {
            BeerRatingDto beerDto = mapper.fromBeerToRatingDto(beer);
            if (!username.equals("")) {
                User user = userService.getUserByUsername(username);
                beerDto.setCurrentUserRating(ratingService.getRatingFromUser(beer.getId(), user.getId()));
            }
            beersDto.add(beerDto);

        }
        return beersDto;
    }

    private void removeIfExistInWishDrankList(String status, int beerId, int userId) {
        if (beersRepository.checkBeerExistInWishDrankList(status, beerId, userId)) {
            beersRepository.removeFromWishDrankList(beerId, userId);
        }
    }

    private String getPictureUrl(BeerDto beerDto) {
        if (beerDto.getFile() != null) {
            if (beerDto.getFile().isEmpty()) {
                return DEFAULT_BEER_IMAGE;
            } else {
                return fileService.uploadFile(beerDto.getFile(),
                        IMAGE_PATH_FOR_BEER + beerDto.getName());
            }
        } else {
            return DEFAULT_BEER_IMAGE;
        }
    }

}
