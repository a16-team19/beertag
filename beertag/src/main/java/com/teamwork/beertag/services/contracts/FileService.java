package com.teamwork.beertag.services.contracts;

import org.springframework.web.multipart.MultipartFile;

public interface FileService {

    String uploadFile(MultipartFile file, String name);
}
