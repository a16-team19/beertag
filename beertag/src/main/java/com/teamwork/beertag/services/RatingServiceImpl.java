package com.teamwork.beertag.services;

import com.teamwork.beertag.models.Beer;
import com.teamwork.beertag.models.User;
import com.teamwork.beertag.models.dtos.RatingDeleteDto;
import com.teamwork.beertag.models.dtos.RatingDto;
import com.teamwork.beertag.repositories.contracts.BeersRepository;
import com.teamwork.beertag.repositories.contracts.RatingRepository;
import com.teamwork.beertag.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements com.teamwork.beertag.services.contracts.RatingService {

    private final RatingRepository ratingRepository;
    private final UserRepository userRepository;
    private final BeersRepository beersRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository, UserRepository userRepository, BeersRepository beersRepository) {
        this.ratingRepository = ratingRepository;
        this.userRepository = userRepository;
        this.beersRepository = beersRepository;
    }

    @Override
    public void addRating(RatingDto ratingDto) {
        User user = userRepository.getUserByUsername(ratingDto.getUsername());
        Beer beer = beersRepository.getByName(ratingDto.getBeerName());
        ratingRepository.addRating(beer.getId(), user.getId(), ratingDto.getRate());
    }

    @Override
    public void deleteUserRating(RatingDeleteDto ratingDeleteDto) {
        User user = userRepository.getUserByUsername(ratingDeleteDto.getUsername());
        Beer beer = beersRepository.getByName(ratingDeleteDto.getBeerName());
        ratingRepository.deleteUserRating(beer.getId(), user.getId());
    }

    @Override
    public Double getRating(int beerId) {
        return ratingRepository.getRating(beerId);
    }

    @Override
    public Integer getNumberOfRatings(int beerId) {
        return ratingRepository.getNumberOfRatings(beerId);
    }

    @Override
    public Integer getRatingFromUser(int beerId, int userId) {
        return ratingRepository.getRatingFromUser(beerId,userId);
    }

}
