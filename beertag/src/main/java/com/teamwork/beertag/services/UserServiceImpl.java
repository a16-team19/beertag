package com.teamwork.beertag.services;

import com.teamwork.beertag.exceptions.DuplicateEntityException;
import com.teamwork.beertag.exceptions.EntityNotFoundException;
import com.teamwork.beertag.models.User;
import com.teamwork.beertag.models.UserDetails;
import com.teamwork.beertag.models.dtos.BeerDto;
import com.teamwork.beertag.models.dtos.DtoMapper;
import com.teamwork.beertag.models.dtos.UserDetailsDto;
import com.teamwork.beertag.repositories.contracts.UserRepository;
import com.teamwork.beertag.services.contracts.FileService;
import com.teamwork.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    public static final String DUPLICATE_USER_NAME = "User with name: %s already exist";
    public static final String IMAGE_PATH_FOR_USER = "users/";
    public static final String DEFAULT_USER_IMAGE = "/images/users/default-avatar.png";

    private final UserRepository userRepository;
    private final DtoMapper mapper;
    private final FileService fileService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, DtoMapper mapper, FileService fileService) {
        this.userRepository = userRepository;
        this.mapper = mapper;
        this.fileService = fileService;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getUserById(int id) {

        return userRepository.getUserById(id);
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public void updateUserById(User user) {
        userRepository.getUserById(user.getId());
        userRepository.updateUser(user);
    }

    @Override
    public void deleteUser(User user) {
        userRepository.deleteUser(user);
    }

    @Override
    public User createUser(User user) {
        if (userRepository.checkUserExist(user.getUsername())) {
            throw new DuplicateEntityException(String.format(DUPLICATE_USER_NAME, user.getUsername()));
        }
        return userRepository.createUser(user);

    }

    @Override
    public void updateUserDetails(String username, UserDetailsDto userDetailsDto) {
        User user = userRepository.getUserByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException("This user does not exist!");
        }
        UserDetails userDetails = mapper.fromUserDetailsDto(userDetailsDto);
        userDetails.setId(user.getUserDetails().getId());

        userDetails.setPictureURL(getPictureUrl(userDetailsDto, username));

        userRepository.updateUserDetails(userDetails);
    }


    @Override
    public void addDefaultUserDetails(String username) {
        User user = userRepository.getUserByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException("This user does not exist!");
        }
        UserDetails userDetails = new UserDetails();
        userDetails.setPictureURL(DEFAULT_USER_IMAGE);

        userRepository.createUserDetails(user.getId(), userDetails);
    }

    private String getPictureUrl(UserDetailsDto userDetailsDto, String username) {
        if (userDetailsDto.getFile() != null) {
            if (userDetailsDto.getFile().isEmpty()) {
                return DEFAULT_USER_IMAGE;
            } else {
                return fileService.uploadFile(userDetailsDto.getFile(),
                        IMAGE_PATH_FOR_USER + username);
            }
        } else {
            return DEFAULT_USER_IMAGE;
        }
    }

}

