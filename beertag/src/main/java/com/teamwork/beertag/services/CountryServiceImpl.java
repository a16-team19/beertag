package com.teamwork.beertag.services;

import com.teamwork.beertag.models.Country;
import com.teamwork.beertag.repositories.contracts.CountryRepository;
import com.teamwork.beertag.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAll() {
        return countryRepository.getAll();
    }

    @Override
    public Country getById(String id) {
        return countryRepository.getById(id);
    }

    @Override
    public void createCountry(Country country) {
        countryRepository.createCountry(country);
    }

    @Override
    public void deleteCountry(Country country) {
        countryRepository.deleteCountry(country);
    }

    @Override
    public void updateCountry(Country country) {
        countryRepository.updateCountry(country);
    }

    @Override
    public boolean checkCountryExist(String name) {
        return countryRepository.checkCountryExist(name);
    }
}
