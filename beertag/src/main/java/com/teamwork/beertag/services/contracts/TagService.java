package com.teamwork.beertag.services.contracts;

import com.teamwork.beertag.models.Tag;

import java.util.Set;

public interface TagService {

    void addTag(int beerId, int tagId);

    Tag getTagById(Integer id);

    Set<Tag> getAll();

    void deleteTag(Tag tag);

    void createTag(Tag tag);

    boolean checkTagExist(String name);

}
