/*$(document).ready(function(){
    $("button").click(function(){
        $.post("http://localhost:8080/v1/countries",
            {
                country_id: $("#code").val(),
                name: $("#countryname").val()
            })
    });
});*/

$(document).ready(function () {

    $("button").click(function () {
        let data = {
            "id": $("#code").val(),
            "name": $("#countryname").val()
        };
        $.ajax({
            url: 'http://localhost:8080/v1/countries',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data)
        });
    });
});
