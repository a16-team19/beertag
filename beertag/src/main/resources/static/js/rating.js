/*$("input[type=radio]").each( function() {
    if ($(this).prop('value') <= $(".rating-value").prop('value') ) {
        $(this).prop('checked','checked');
        return false;
    }
});*/

$(".rate").each(function() {
    let $rating =Math.round($(this).find(".rating-value").prop("value"));
    $(this).find("#star" + $rating + $(this).closest('.card-body').find('h4').text()).prop('checked','checked');
});

$(document).ready(function () {
    $('input[type="radio"]').click(function() {
        let jsonData = {
            "beerName": $(this).closest('.card-body').find('h4').text().trim(),
            "rate": $(this).attr("value")
        };
        $.ajax({
            url: 'http://localhost:8080/beers/rate',
            type: 'POST',
            contentType: 'application/json',
            success: function() {
                location.reload();
            },
            error: function(xhr) {
                if (xhr.status === 400) {
                    if (confirm('You already rated ' + jsonData.beerName + ' do you want to reset your rating for ' + jsonData.beerName + '?')) {
                        let jsonDelete = {
                            "beerName": jsonData.beerName
                        };
                        $.ajax({
                            url: 'http://localhost:8080/beers/rate',
                            type: 'DELETE',
                            contentType: 'application/json',
                            success: function() {
                                location.reload();
                            },
                            error: function(){
                                location.reload();
                            },
                            data: JSON.stringify(jsonDelete)
                        });

                        } else {
                        // Do nothing!
                    }
                }
            },
            data: JSON.stringify(jsonData)
        });
    });
});