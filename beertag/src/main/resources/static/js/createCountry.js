$(document).ready(function () {

    $("#button").click(function () {
        let data = {
            "id": $("#code").val(),
            "name": $("#countryname").val()
        };
        $.ajax({
            url: 'http://localhost:8080/country/create',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data)
        });
    });
});