$(".rate").each(function() {
    let $rating =Math.round($(this).find(".rating-value").prop("value"));
    $(this).find("#star" + $rating + $(this).closest('.details').find('h3').text()).prop('checked','checked');
});


$(document).ready(function () {
    $('input[type="radio"]').click(function() {
        let jsonData = {
            "beerName": $(this).closest('.details').find('h3').text().trim(),
            "rate": $(this).attr("value")
        };
        $.ajax({
            url: 'http://localhost:8080/beers/rate',
            type: 'POST',
            contentType: 'application/json',
            success: function() {
                location.reload();
            },
            error: function(xhr) {
                if (xhr.status === 400) {
                    if (confirm('You already rated ' + jsonData.beerName + ' do you want to reset your rating for ' + jsonData.beerName + '?')) {
                        let jsonDelete = {
                            "beerName": jsonData.beerName
                        };
                        $.ajax({
                            url: 'http://localhost:8080/beers/rate',
                            type: 'DELETE',
                            contentType: 'application/json',
                            success: function() {
                                location.reload();
                            },
                            error: function(){
                                location.reload();
                            },
                            data: JSON.stringify(jsonDelete)
                        });
                        location.reload();
                    } else {
                        // Do nothing!
                    }
                }
            },
            data: JSON.stringify(jsonData)
        });
    });
});