package com.teamwork.beertag;

import com.teamwork.beertag.models.*;
import com.teamwork.beertag.models.dtos.*;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Factory {

    public static final String STYLE_NAME = "styleName";
    public static final String BREWERY_NAME = "breweryName";
    public static final String COUNTRY_ID = "countryId";
    public static final String COUNTRY_NAME = "countryName";
    public static final String USER_NAME = "userName";
    public static final String BEER_NAME = "beerName";
    public static final String DEFAULT_BEER_IMAGE_ADDRESS = "/images/beers/default-beer.jpg";
    public static final String TAG_NAME = "tagName";
    public static final String UPDATED_NAME = "updatedName";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";


    public static Style createStyle() {
        Style style = new Style();
        style.setId(1);
        style.setName(STYLE_NAME);

        return style;
    }

    public static Brewery createBrewery() {
        Brewery brewery = new Brewery();
        brewery.setId(1);
        brewery.setName(BREWERY_NAME);

        return brewery;
    }

    public static Country createCountry() {
        Country country = new Country();
        country.setId(COUNTRY_ID);
        country.setName(COUNTRY_NAME);

        return country;
    }

    public static User createUser() {
        User user = new User();
        user.setUsername(USER_NAME);
        user.setUserDetails(new UserDetails());

        return user;
    }

    public static Beer createBeer() {
        Beer beer = new Beer();
        beer.setName(BEER_NAME);
        beer.setStyle(createStyle());
        beer.setBrewery(createBrewery());
        beer.setCountry(createCountry());
        beer.setCreatedBy(createUser());
        beer.setTags(new HashSet<>());
        beer.setPictureUrl(DEFAULT_BEER_IMAGE_ADDRESS);

        return beer;
    }

    public static BeerDto createBeerDto() {
        BeerDto beer = new BeerDto();
        beer.setName(BEER_NAME);
        beer.setStyleId(createStyle().getId());
        beer.setBreweryId(createBrewery().getId());
        beer.setCountryId(createCountry().getId());
        beer.setCreatedBy(createUser().getUsername());
        beer.setTags(new ArrayList<>());
        return beer;
    }

    public static BeerRatingDto createBeerRatingDto() {
        BeerRatingDto beer = new BeerRatingDto();
        beer.setName(BEER_NAME);
        beer.setStyle(createStyle());
        beer.setBrewery(createBrewery());
        beer.setCountry(createCountry());
        beer.setUser(createUser());
        beer.setTags(new HashSet<>());
        beer.setPictureUrl(DEFAULT_BEER_IMAGE_ADDRESS);
        beer.setRating(0.0);

        return beer;
    }

    public static Tag createTag() {
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName(TAG_NAME);

        return tag;
    }

    public static BeerSortObjDto createBeerSortObjDto() {
        BeerSortObjDto beer = new BeerSortObjDto();
        beer.setName(BEER_NAME);
        beer.setStyle(createStyle());
        beer.setBrewery(createBrewery());
        beer.setCountry(createCountry());
        beer.setTag(createTag());

        return beer;
    }

    public static BeerSortDto createBeerSortDto() {
        BeerSortDto beer = new BeerSortDto();
        beer.setName(BEER_NAME);
        beer.setStyleId(createStyle().getId());
        beer.setBreweryId(createBrewery().getId());
        beer.setCountryId(createCountry().getId());
        beer.setRating(0);

        return beer;
    }

    public static UserDetails createUserDetails(){
        UserDetails userDetails = new UserDetails();
        userDetails.setId(1);
        userDetails.setFirstName(FIRST_NAME);
        userDetails.setLastName(LAST_NAME);

        return userDetails;
    }

    public static UserDetailsDto createUserDetailsDto(){
        UserDetailsDto userDetails = new UserDetailsDto();
        userDetails.setFirstName(FIRST_NAME);
        userDetails.setLastName(LAST_NAME);

        return userDetails;
    }

    public static RatingDto createRatingDto(){
        RatingDto ratingDto = new RatingDto();
        ratingDto.setBeerName(BEER_NAME);
        ratingDto.setUsername(USER_NAME);
        ratingDto.setRate(1);

        return ratingDto;
    }

    public static RatingDeleteDto createRatingDeleteDto(){
        RatingDeleteDto ratingDto = new RatingDeleteDto();
        ratingDto.setBeerName(BEER_NAME);
        ratingDto.setUsername(USER_NAME);

        return ratingDto;
    }

    public static MultipartFile createFile(){
        MultipartFile file = new MockMultipartFile("file","fileName","txt", new byte[0]);

        return file;
    }

}
