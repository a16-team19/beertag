package com.teamwork.beertag.services;

import com.teamwork.beertag.models.Brewery;
import com.teamwork.beertag.models.Country;
import com.teamwork.beertag.repositories.contracts.BreweryRepository;
import com.teamwork.beertag.repositories.contracts.CountryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.teamwork.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository countryRepository;

    @InjectMocks
    CountryServiceImpl countryService;

    @Test
    public void getShould_ReturnAllCountries_WhenExists() {
        //Arrange

        Mockito.when(countryRepository.getAll()).thenReturn(Arrays.asList(
                createCountry(),
                createCountry()
        ));

        // Act
        List<Country> result = countryService.getAll();

        // Assert
        Assert.assertSame(COUNTRY_NAME, (result.get(0)).getName());
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getByIdShould_ReturnCountry_WhenExist() {
        // Arrange
        Country expected = createCountry();
        Mockito.when(countryRepository.getById(COUNTRY_ID)).thenReturn(createCountry());

        // act
        Country result = countryService.getById(COUNTRY_ID);

        // Assert
        Assert.assertSame(expected.getName(), result.getName());
    }

    @Test
    public void createCountryShould_CreateBrewery() {
        // Arrange
        Country country = createCountry();

        // Act
        countryService.createCountry(country);

        // Assert
        Mockito.verify(countryRepository, Mockito.times(1)).createCountry(country);
    }

    @Test
    public void deleteCountryShould_DeleteBrewery() {
        // Arrange
        Country country = createCountry();

        // Act
        countryService.deleteCountry(country);

        // Assert
        Mockito.verify(countryRepository, Mockito.times(1)).deleteCountry(country);
    }

    @Test
    public void updateCountryShould_UpdateName() {

        // Arrange Act
        countryService.updateCountry(createCountry());

        // Assert
        Mockito.verify(countryRepository, Mockito.times(1))
                .updateCountry(any(Country.class));
    }
}
