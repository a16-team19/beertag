package com.teamwork.beertag.services;

import com.teamwork.beertag.models.Tag;
import com.teamwork.beertag.repositories.contracts.TagRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;
import java.util.Set;

import static com.teamwork.beertag.Factory.createTag;

@RunWith(MockitoJUnitRunner.class)

public class TagServiceImplTests {

    @Mock
    TagRepository tagRepository;

    @InjectMocks
    TagServiceImpl tagService;

    @Test
    public void getShould_ReturnAllTags_WhenExists() {
        //Arrange
        Set<Tag> tags = new HashSet<>();
        tags.add(createTag());
        Mockito.when(tagRepository.getAll()).thenReturn(tags);

        // Act
        Set<Tag> result =  tagService.getAll();

        // Assert
        Assert.assertSame(false, (result.isEmpty()));
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getByIdShould_ReturnTag_WhenExist() {
        // Arrange
        Tag expected = createTag();
        Mockito.when(tagRepository.getTagById(1)).thenReturn(createTag());

        // act
        Tag result = tagService.getTagById(1);

        // Assert
        Assert.assertSame(expected.getName(), result.getName());
    }

    @Test
    public void createTagShould_CreateTag() {
        // Arrange
        Tag tag = createTag();

        // Act
        tagService.createTag(tag);

        // Assert
        Mockito.verify(tagRepository, Mockito.times(1))
                .createTag(tag);
    }

    @Test
    public void deleteTagShould_DeleteTag() {
        // Arrange
        Tag tag = createTag();

        // Act
        tagService.deleteTag(tag);

        // Assert
        Mockito.verify(tagRepository, Mockito.times(1))
                .deleteTag(tag);
    }



}
