package com.teamwork.beertag.services;

import com.teamwork.beertag.exceptions.DuplicateEntityException;
import com.teamwork.beertag.models.Style;
import com.teamwork.beertag.repositories.contracts.StylesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.teamwork.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {

    @Mock
    StylesRepository stylesRepository;

    @InjectMocks
    StyleServiceImpl styleService;

    @Test
    public void getShould_ReturnAllStyles_WhenExists() {
        //Arrange

        Mockito.when(stylesRepository.getAll()).thenReturn(Arrays.asList(
                createStyle(),
                createStyle()
        ));

        // Act
        List<Style> result = styleService.getAll();

        // Assert
        Assert.assertSame(STYLE_NAME, (result.get(0)).getName());
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getByIdShould_ReturnStyle_WhenExist() {
        // Arrange
        Style expected = createStyle();
        Mockito.when(stylesRepository.getById(1)).thenReturn(createStyle());

        // act
        Style result = styleService.getById(1);

        // Assert
        Assert.assertSame(expected.getName(), result.getName());
    }

    @Test
    public void createStyleShould_CreateStyle() {
        // Arrange
        Style style = createStyle();

        // Act
        styleService.createStyle(style);

        // Assert
        Mockito.verify(stylesRepository, Mockito.times(1))
                .createStyle(style);
    }

    @Test
    public void deleteStyleShould_DeleteStyle() {
        // Arrange
        Style style = createStyle();

        // Act
        styleService.deleteStyle(style);

        // Assert
        Mockito.verify(stylesRepository, Mockito.times(1))
                .deleteStyle(style);
    }

    @Test
    public void updateStyleShould_UpdateName() {

        // Arrange Act
        styleService.updateStyle(createStyle());

        // Assert
        Mockito.verify(stylesRepository, Mockito.times(1))
                .updateStyle(any(Style.class));
    }

    @Test
    public void checkStyleExistShould_Throw() {
        // Arrange
        Mockito.when(styleService.checkStyleExist(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> styleService.createStyle(createStyle()));
    }
}
