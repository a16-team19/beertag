package com.teamwork.beertag.services;

import com.teamwork.beertag.exceptions.DuplicateEntityException;
import com.teamwork.beertag.models.*;
import com.teamwork.beertag.models.dtos.*;
import com.teamwork.beertag.repositories.contracts.*;
import com.teamwork.beertag.services.contracts.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.teamwork.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {


    @Mock
    BeersRepository beersRepository;

    @Mock
    DtoMapper dtoMapper;

    @Mock
    UserService userService;


    @InjectMocks
    BeersServiceImpl beersService;


    @Test
    public void createShould_Throw_WhenBeerAlreadyExist() {

        //Arrange
        Mockito.when(beersRepository.checkBeerExists(anyString()))
                .thenReturn(true);


        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> beersService.createBeer(createBeerDto()));
    }


    @Test
    public void CreateBeerShould_CreateBeer() {

        // ARRANGE
        Mockito.when(dtoMapper.fromBeerDto(any(BeerDto.class))).thenReturn(createBeer());
        Mockito.when(beersRepository.checkBeerExists(anyString())).thenReturn(false);
        Mockito.when(userService.getUserByUsername(anyString())).thenReturn(createUser());
        Beer beerToCompare = createBeer();


        // Act
        Beer beer = beersService.createBeer(createBeerDto());

        // ASSERT
        Assert.assertEquals(beerToCompare.getName(), beer.getName());
        Assert.assertEquals(beerToCompare.getPictureUrl(), beer.getPictureUrl());
    }

    @Test
    public void getByNameShould_ReturnBeer_WhenBeersExist() {
        //Arrange
        Beer expectedBeer = createBeer();

        Mockito.when(beersRepository.getByName(anyString())).thenReturn(expectedBeer);

        //Act
        Beer returnedBeer = beersService.getByName(anyString());

        //Assert
        Assert.assertSame(expectedBeer, returnedBeer);

    }

    @Test
    public void getWishListShould_ReturnBeerRatingDto_WhenBeersExist() {
        // Arrange
        Mockito.when(beersRepository.getWishOrDrankList(anyInt(), anyString())).thenReturn(Arrays.asList(
                createBeer(),
                createBeer()
        ));
        Mockito.when(userService.getUserByUsername(anyString())).thenReturn(createUser());
        Mockito.when(dtoMapper.fromBeerToRatingDto(any(Beer.class))).thenReturn(createBeerRatingDto());

        // Act
        List<BeerRatingDto> result = beersService.getWishList(anyString());

        //Assert
        Assert.assertSame(BEER_NAME, (result.get(0)).getName());
        Assert.assertEquals(2,result.size());
    }

    @Test
    public void addToWishList() {
        // Arrange
        User user = createUser();
        BeerRatingDto expectedBeer = createBeerRatingDto();

        Mockito.when(userService.getUserByUsername(anyString())).thenReturn(createUser());
        Mockito.when(dtoMapper.fromBeerToRatingDto(any(Beer.class))).thenReturn(createBeerRatingDto());
        Mockito.when(beersRepository.getWishOrDrankList(anyInt(), anyString())).thenReturn(Arrays.asList(
                createBeer(),
                createBeer()
        ));

        // Act
        beersService.addToWishList(user.getUsername(), anyInt());
        BeerRatingDto result = beersService.getWishList(user.getUsername()).get(0);

        //Assert
        Assert.assertSame("beerName", result.getName());

    }

    @Test
    public void getDrankListShould_ReturnBeerRatingDto_WhenBeersExist() {
        // Arrange
        Mockito.when(beersRepository.getWishOrDrankList(anyInt(), anyString())).thenReturn(Arrays.asList(
                createBeer(),
                createBeer()
        ));
        Mockito.when(userService.getUserByUsername(anyString())).thenReturn(createUser());
        Mockito.when(dtoMapper.fromBeerToRatingDto(any(Beer.class))).thenReturn(createBeerRatingDto());

        // Act
        List<BeerRatingDto> result = beersService.getDrankList(anyString());

        //Assert
        Assert.assertSame(BEER_NAME, (result.get(0)).getName());
        Assert.assertEquals(2,result.size());
    }

    @Test
    public void addToDrankListShould_AddCorrectBeer() {
        // Arrange
        User user = createUser();
        BeerRatingDto expectedBeer = createBeerRatingDto();

        Mockito.when(userService.getUserByUsername(anyString())).thenReturn(createUser());
        Mockito.when(dtoMapper.fromBeerToRatingDto(any(Beer.class))).thenReturn(createBeerRatingDto());
        Mockito.when(beersRepository.getWishOrDrankList(anyInt(), anyString())).thenReturn(Arrays.asList(
                createBeer(),
                createBeer()
        ));

        // Act
        beersService.addToDrankList(user.getUsername(), anyInt());
        BeerRatingDto result = beersService.getDrankList(user.getUsername()).get(0);

        //Assert
        Assert.assertSame("beerName", result.getName());
    }

    @Test
    public void getTop4BeersShould_ReturnTop4Beers_WhenExist() {
        // Arrange
        Mockito.when(beersRepository.getTop4Beers()).thenReturn(Arrays.asList(
                createBeer(),
                createBeer()
        ));
        Mockito.when(dtoMapper.fromBeerToRatingDto(any(Beer.class))).thenReturn(createBeerRatingDto());


        // Act
        List<BeerRatingDto> result = beersService.getTop4Beers("");

        //Arrange
        Assert.assertSame(BEER_NAME, result.get(0).getName());
        Assert.assertEquals(2,result.size());
    }

    @Test
    public void getAllShould_ReturnAll_WhenExist(){
        //Arrange
        Mockito.when(beersRepository.getAll()).thenReturn(Arrays.asList(
                createBeer(),
                createBeer()
        ));
        Mockito.when(dtoMapper.fromBeerToRatingDto(any(Beer.class))).thenReturn(createBeerRatingDto());


        // Act
        List<BeerRatingDto> result = beersService.getAll();

        //Arrange
        Assert.assertSame(BEER_NAME, result.get(0).getName());
        Assert.assertEquals(2,result.size());
    }

    @Test
    public void filterBeersByNameShould_ReturnBeers_WhenExists(){
        //Arrange
        Mockito.when(beersRepository.filterBeersByName(anyString())).thenReturn(Arrays.asList(
                createBeer(),
                createBeer()
        ));
        Mockito.when(dtoMapper.fromBeerToRatingDto(any(Beer.class))).thenReturn(createBeerRatingDto());

        // Act
        List<BeerRatingDto> result = beersService.filterBeersByName(anyString(),"");

        //Arrange
        Assert.assertSame(BEER_NAME, result.get(0).getName());
        Assert.assertEquals(2,result.size());
    }

    @Test
    public void filterBeersShould_ReturnBeers_IfExists(){

        //Arrange
        Mockito.when(beersRepository.filterBeers(any(BeerSortObjDto.class))).thenReturn(Arrays.asList(
                createBeer(),
                createBeer()
        ));
        Mockito.when(dtoMapper.fromBeerToRatingDto(any(Beer.class))).thenReturn(createBeerRatingDto());
        Mockito.when(dtoMapper.getSortBeer(any(BeerSortDto.class))).thenReturn(createBeerSortObjDto());

        // Act
        List<BeerRatingDto> result = beersService.filterBeers(createBeerSortDto(),"");

        //Arrange
        Assert.assertSame(BEER_NAME, result.get(0).getName());
        Assert.assertEquals(2,result.size());
    }

    @Test
    public void updateShould_UpdateBeer_WhenExist(){
        //Arrange
        Beer expected = createBeer();
        expected.setName(UPDATED_NAME);
        Mockito.when(dtoMapper.fromBeerDto(any(BeerDto.class))).thenReturn(createBeer());
        Mockito.when(beersService.getByName(anyString())).thenReturn(createBeer());
        BeerDto beerDto = new BeerDto();
        beerDto.setName(UPDATED_NAME);

        // Act
        beersService.updateBeer(beerDto);
        Beer result = beersService.getByName(UPDATED_NAME);

        //Arrange
        Assert.assertSame(BEER_NAME, result.getName());
    }
}
