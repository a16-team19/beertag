package com.teamwork.beertag.services;

import com.teamwork.beertag.Factory;
import com.teamwork.beertag.models.Brewery;
import com.teamwork.beertag.repositories.contracts.BreweryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.teamwork.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class BreweryServiceImplTest {

    @Mock
    BreweryRepository breweryRepository;

    @InjectMocks
    BreweryServiceImpl breweryService;

    @Test
    public void getShould_ReturnAllBreweries_WhenExists() {
        //Arrange

        Mockito.when(breweryRepository.getAll()).thenReturn(Arrays.asList(
                createBrewery(),
                createBrewery()
        ));

        // Act
        List<Brewery> result = breweryService.getAll();

        // Assert
        Assert.assertSame(BREWERY_NAME, (result.get(0)).getName());
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getByIdShould_ReturnBrewery_WhenExist() {
        // Arrange
        Brewery expected = createBrewery();
        Mockito.when(breweryRepository.getById(anyInt())).thenReturn(createBrewery());

        // act
        Brewery result = breweryService.getById(anyInt());

        // Assert
        Assert.assertSame(expected.getId(), result.getId());
        Assert.assertSame(expected.getName(), result.getName());
    }

    @Test
    public void createBreweryShould_CreateBrewery() {
        // Arrange
        Brewery expected = createBrewery();
        Mockito.when(breweryRepository.createBrewery(any(Brewery.class))).thenReturn(createBrewery());

        // Act
        Brewery result = breweryService.createBrewery(createBrewery());

        // Assert
        Assert.assertSame(expected.getName(), result.getName());
    }

    @Test
    public void deleteBreweryShould_DeleteBrewery() {
        // Arrange
        Brewery brewery = createBrewery();

        // Act
        breweryService.deleteBrewery(brewery);

        // Assert
        Mockito.verify(breweryRepository, Mockito.times(1)).deleteBrewery(brewery);
    }

    @Test
    public void updateBreweryShould_UpdateName() {

        // Arrange Act
        breweryService.updateBrewery(createBrewery());

        // Assert
        Mockito.verify(breweryRepository, Mockito.times(1))
                .updateBrewery(any(Brewery.class));
    }
}
