package com.teamwork.beertag.services;

import com.teamwork.beertag.repositories.RatingRepositoryImpl;
import com.teamwork.beertag.repositories.contracts.BeersRepository;
import com.teamwork.beertag.repositories.contracts.RatingRepository;
import com.teamwork.beertag.repositories.contracts.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.teamwork.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)

public class RatingServiceImplTests {

    public static final double RATING = 5.5;
    @Mock
    RatingRepository ratingRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    BeersRepository beersRepository;

    @InjectMocks
    RatingServiceImpl ratingService;

    @Test
    public void addRatingShould_CallRepository(){
        // Arrange
        Mockito.when(userRepository.getUserByUsername(anyString())).thenReturn(createUser());
        Mockito.when(beersRepository.getByName(anyString())).thenReturn(createBeer());
        // Act
       ratingService.addRating(createRatingDto());

       //Assert
        Mockito.verify(ratingRepository,Mockito.times(1)).addRating(anyInt(),anyInt(),anyInt());
    }

    @Test
    public void deleteUserRatingShould_CallRepository(){
        // Arrange
        Mockito.when(userRepository.getUserByUsername(anyString())).thenReturn(createUser());
        Mockito.when(beersRepository.getByName(anyString())).thenReturn(createBeer());

        // Act
        ratingService.deleteUserRating(createRatingDeleteDto());

        //Assert
        Mockito.verify(ratingRepository,Mockito.times(1))
                .deleteUserRating(anyInt(),anyInt());
    }

    @Test
    public void getRatingShould_ReturnRating_whenExist(){
        // Arrange
        Double expected = RATING;
        Mockito.when(ratingRepository.getRating(anyInt()))
                .thenReturn(RATING);

        // Act
        Double result = ratingService.getRating(1);

        // Assert
        Assert.assertEquals(expected,result);
    }

    @Test
    public void getNumberOfRatingsShould_ReturnNumberOfRatings_WhenExist(){
        // Arrange
        int expected = 1;
        Mockito.when(ratingRepository.getNumberOfRatings(anyInt()))
                .thenReturn(1);

        //Act
        int result = ratingService.getNumberOfRatings(anyInt());

        // Assert
        Assert.assertEquals(expected,result);
    }
}
