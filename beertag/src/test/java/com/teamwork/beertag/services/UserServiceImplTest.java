package com.teamwork.beertag.services;

import com.teamwork.beertag.models.User;
import com.teamwork.beertag.models.UserDetails;
import com.teamwork.beertag.models.dtos.DtoMapper;
import com.teamwork.beertag.models.dtos.UserDetailsDto;
import com.teamwork.beertag.repositories.contracts.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static com.teamwork.beertag.Factory.*;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    UserRepository userRepository;

    @Mock
    DtoMapper dtoMapper;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void getShould_ReturnAllUsers_WhenExists() {
        //Arrange

        Mockito.when(userRepository.getAll()).thenReturn(Arrays.asList(
                createUser(),
                createUser()
        ));

        // Act
        List<User> result = userService.getAll();

        // Assert
        Assert.assertSame(USER_NAME, (result.get(0)).getUsername());
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getByIdShould_ReturnUser_WhenExist() {
        // Arrange
        User expected = createUser();
        Mockito.when(userRepository.getUserByUsername(anyString())).thenReturn(createUser());

        // act
        User result = userService.getUserByUsername(anyString());

        // Assert
        Assert.assertSame(expected.getId(), result.getId());
        Assert.assertSame(expected.getUsername(), result.getUsername());
    }

    @Test
    public void deleteUserShould_DeleteUser() {
        // Arrange
        User user = createUser();

        // Act
        userService.deleteUser(user);

        // Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .deleteUser(user);
    }

    @Test
    public void createUserShould_CreateUser() {
        // Arrange
        User user = createUser();

        // Act
        userService.createUser(user);

        // Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .createUser(user);
    }

    @Test
    public void updateBreweryShould_UpdateName() {

        // Arrange Act
        userService.updateUserById(createUser());

        // Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .updateUser(any(User.class));
    }

    @Test
    public void updateUserDetailsShould_UpdateUserDetails(){
        // Arrange
        Mockito.when(userRepository.getUserByUsername(anyString()))
                .thenReturn(createUser());
        Mockito.when(dtoMapper.fromUserDetailsDto(any(UserDetailsDto.class)))
                .thenReturn(createUserDetails());

        // Act
        userService.updateUserDetails(USER_NAME,createUserDetailsDto());

        // Assert
        Mockito.verify(userRepository,Mockito.times(1))
                .updateUserDetails(any(UserDetails.class));
    }

    @Test
    public void addDefaultUserDetailsShould_UpdateUserDetails(){
        // Arrange
        Mockito.when(userRepository.getUserByUsername(anyString()))
                .thenReturn(createUser());

        // Act
        userService.addDefaultUserDetails(USER_NAME);

        // Assert
        Mockito.verify(userRepository,Mockito.times(1))
                .createUserDetails(anyInt(),any(UserDetails.class));
    }

}
